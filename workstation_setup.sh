#!/bin/bash

set -ex -o pipefail

# Validate that the backup location is passed.
if [ "$#" -ne 1 ]
then
   echo "ERROR: You must provide the backup location as an argument to this script."
fi

# File path variables.
BACKUP_LOCATION="$1"
PYTHON_UTIL_DIR="codebase/python/utils"
PACKAGES_DIR="packages"

# Update all packages before doing anything else.
sudo dnf upgrade --refresh -y

mkdir -p "$HOME/$PYTHON_UTIL_DIR"

# Copy files and directories from the backup location
cp -r "$BACKUP_LOCATION/home/.crypto" ~
cp "$BACKUP_LOCATION/$PYTHON_UTIL_DIR/ssh-add-askpass.py" "$HOME/$PYTHON_UTIL_DIR/ssh-add-askpass.py"

# Download .git-prompt.sh and save it in the home directory
curl -s -o "$HOME/.git-prompt.sh" "https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh"

# Copy config files
cp configs/bash/bash_profile ~/.bash_profile
cp configs/bash/bashrc ~/.bashrc
cp configs/vim/vimrc ~/.vimrc

# Source the .bashrc
source ~/.bashrc

# Install rpmfusion repos
sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

# Install dnf packages
sudo dnf install --refresh -y $(cat "$PACKAGES_DIR/dnf_packages.txt")
sudo dnf group install -y "C Development Tools and Libraries"

### vscode

# Install vscode
# Adapted from: https://code.visualstudio.com/docs/setup/linux#_rhel-fedora-and-centos-based-distributions
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo cp configs/vscode/vscode.repo /etc/yum.repos.d/vscode.repo
sudo dnf install --refresh -y code

# Install vscode extensions
while read EXTENSION; do
    code --install-extension "$EXTENSION"
done < "$PACKAGES_DIR/vscode_extensions.txt"

# Copy vscode config files
cp configs/vscode/*.json ~/.config/Code/User

### Python

# Install pipx
python -m pip install --no-input --user pipx
python -m pipx ensurepath

# Install global Python tools as pipx packages
# Install each pacakge in turn, since pipx only supports installing
# one package at a time.
while read PACKAGE; do
    pipx install "$PACKAGE"
done < "$PACKAGES_DIR/pipx_packages.txt"

### Ruby
gem install bundler

### Post-install instructions

echo "The workspace setup script finished successfully! Perform the following steps manually."

PASSWORDSAFE_URL="https://sourceforge.net/projects/passwordsafe/files/Linux"
SLACK_URL="https://slack.com/downloads/linux"
SELENIUM_URL="https://www.selenium.dev/downloads"
GECKODRIVER_URL="https://github.com/mozilla/geckodriver/releases/latest"
echo -e "Download and install the latest passwordsafe RPM from here:\n$PASSWORDSAFE_URL"
echo -e "Download and install the latest Slack RPM from here:\n$SLACK_URL"
echo -e "Download the latest Selenium jar from here, and copy it to ~/bin:\n$SELENIUM_URL"
echo -e "Download the latest geckodriver executable from here, and copy it to ~/bin:\n$GECKODRIVER_URL"
