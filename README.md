# Workstation Setup

Personal setup scripts and configs.

### Upgrade all Packages

```bash
sudo dnf upgrade --refresh -y
```

### Generate an SSH key

Connect the backup drive and mount it.

Get the path of the mounted backup drive, e.g. `/run/media/josh/My Passport/backup`.

Generate an SSH key pair:
```bash
ssh-keygen -t ed25519 -C "jvernon_483@hotmail.com"
```

Accept the default location for the key file, and create your password:
```
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/user/.ssh/id_ed25519):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
```

### Add the SSH key to GitLab

Install xclip if it's not installed already.
```
sudo dnf install xclip
```

Copy the key to the clipboard:
```bash
xclip -sel clip < ~/.ssh/id_ed25519.pub
```

[Add the key to GitLab.](https://docs.gitlab.com/ee/ssh/#adding-an-ssh-key-to-your-gitlab-account)

### Configure git and clone this project

Download the `.gitconfig` file from this repo, saving it to your home directory:
```bash
curl -o ~/.gitconfig "https://gitlab.com/jvenom/workstation-setup/-/raw/master/configs/git/gitconfig"
```

Clone this project:
```bash
git clone git@gitlab.com:jvenom/workstation-setup.git
```

### Run the setup script

Change to the workspace-setup directory. Run the `workstation_setup.sh` script, passing the backup location as an argument to the script.

```bash
cd workspace-setup
./workstation_setup.sh "/run/media/josh/My Passport/backup"
```

### A note on packages

Packages are listed in the text files in the `packages` directory. Each file is named based on the type of package it contains - dnf packages, vscode extensions, etc. The packages in each file are in alphabetical order. These files are read by the `workstation_setup` script to install the packages.
